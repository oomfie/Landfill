#!/bin/bash
env TMPDIR="$XDG_RUNTIME_DIR/app/${FLATPAK_ID:-net.rptools.maptool}" /app/bin/bin/MapTool $FLAGS "$@"
