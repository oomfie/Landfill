#!/bin/bash

echo "


$$$$$$$$\  $$$$$$\              $$\                                        $$\                     $$\
$$  _____|$$  __$$\             $$ |                                       $$ |                    $$ |
$$ |      $$ /  $$ |$$\   $$\ $$$$$$\    $$$$$$\  $$$$$$\$$$$\   $$$$$$\ $$$$$$\    $$$$$$\   $$$$$$$ |
$$$$$\    $$$$$$$$ |$$ |  $$ |\_$$  _|  $$  __$$\ $$  _$$  _$$\  \____$$\\_$$  _|  $$  __$$\ $$  __$$ |
$$  __|   $$  __$$ |$$ |  $$ |  $$ |    $$ /  $$ |$$ / $$ / $$ | $$$$$$$ | $$ |    $$$$$$$$ |$$ /  $$ |
$$ |      $$ |  $$ |$$ |  $$ |  $$ |$$\ $$ |  $$ |$$ | $$ | $$ |$$  __$$ | $$ |$$\ $$   ____|$$ |  $$ |
$$ |      $$ |  $$ |\$$$$$$  |  \$$$$  |\$$$$$$  |$$ | $$ | $$ |\$$$$$$$ | \$$$$  |\$$$$$$$\ \$$$$$$$ |
\__|      \__|  \__| \______/    \____/  \______/ \__| \__| \__| \_______|  \____/  \_______| \_______|


                                                                                                       "
sleep 2  # Waits 2 seconds.
echo "Adding Flathub And Flathub Beta"
flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo && flatpak remote-add flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo # adds flathub
sleep 3 # waits 3 seconds.
echo "Installing Flatpak Packages"
flatpak install flathub chat.schildi.desktop chat.revolt.RevoltDesktop io.github.spacingbat3.webcord io.github.kotatogram org.polymc.PolyMC com.valvesoftware.Steam com.stremio.Stremio org.videolan.VLC org.keepassxc.KeePassXC com.nextcloud.desktopclient.nextcloud io.github.Figma_Linux.figma_linux
sleep 3 # wait 3 seconds
echo "Installing Packages"
dnf install fish android-tools hplip-gui kde-connect
sleep 3 # wait 3 seconds
echo "Removing packages"
dnf remove plymouth kmail 
