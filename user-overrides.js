// Project Headquarters //

// RANDOM OVERRIDES //
user_pref("widget.use-xdg-desktop-portal.file-picker", 1); // force Firefox to use KDE filepicker
 user_pref("browser.xul.error_pages.expert_bad_cert", true); // adds acception thingy

 // DRM CONTENT //
 user_pref{"browser.eme.enable", true}; // 2022: Enable all DRM content
 user_pref{"browser.eme.ui.enable", true}; // 2022: Enable all DRM content
 user_pref{"security.OCSP.require", false};
 user_pref{"security.OCSP.enabled", 0};
 user_pref("security.ssl.require_safe_negotiation", false);
 user_pref("security.tls.version.enable-deprecated", true);

 // CHNAGE TO FIX VIDEO STREAMING ON SOME SITES //
 user_pref("network.http.referer.XOriginPolicy", 1); // 1601: control when to send a cross-origin referer
 user_pref("network.http.referer.XOriginTrimmingPolicy", 0); // 1602: control the amount of cross-origin information to send [FF52+]

 // DISABLE FORM AUTOFILL //
 user_pref("extensions.formautofill.addresses.enabled", false); // 0811: disable Form Autofill [FF55+]
 user_pref("extensions.formautofill.available", "off"); // 0811: [FF56+]
 user_pref("extensions.formautofill.creditCards.available", false); // 0811: disable Form Autofill [FF57+]
 user_pref("extensions.formautofill.creditCards.enabled", false); // 0811: disable Form Autofill [FF56+]
 user_pref("extensions.formautofill.heuristics.enabled", false); // 0811: disable Form Autofill [FF55+]

 // ETP (ENHANCED TRACKING PROTECTION) //
 user_pref("browser.contentblocking.category", "custom"); // 2701: use ETP custom mode
 user_pref("network.cookie.lifetimePolicy", 2); // 2801: delete cookies and site data on exit
 user_pref("privacy.firstparty.isolate", false); // 6008: default false arkenfox v96
 user_pref("network.cookie.cookieBehavior", 5); // 2701: inactive v96 [set at runtime by category=strict]
 user_pref("privacy.firstparty.isolate", false); // 6008: enforce no First Party Isolation [FF51+]
 user_pref("webgl.disabled", false); // 4520: disable WebGL (Web Graphics Library)
 user_pref("extensions.pocket.enabled", false); // 9000: disable Pocket Account [FF46+]
 user_pref("privacy.resistFingerprinting.letterboxing", true); // 4504: enable RFP letterboxing [FF67+]
 user_pref("privacy.resistFingerprinting.letterboxing.dimensions", "1366x768"); // 4504: enable RFP letterboxing [FF67+]

 // WEBRTC //
 user_pref("media.peerconnection.enabled", true); // 2001: Enable Webrtc

 // VIDEO ENCODING //
 user_pref("gfx.webrender.all", true);
 user_pref("media.hardware-video-decoding.force-enabled", true);
 user_pref("media.ffmpeg.vaapi.enabled", true);
 
